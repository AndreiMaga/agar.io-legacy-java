package net;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import objects.Point;
import org.joml.Vector3f;

public class PointSerializer extends Serializer<Point> {
  @Override
  public void write(Kryo kryo, Output output, Point object) {
    output.writeInt(object.pid);
    output.writeFloat(object.posOnGrid.x);
    output.writeFloat(object.posOnGrid.y);
    output.writeFloat(object.posOnGrid.z);
  }

  @Override
  public Point read(Kryo kryo, Input input, Class<Point> type) {
    return new Point(input.readInt(), new Vector3f(input.readFloat(), input.readFloat(), input.readFloat()));
  }
}