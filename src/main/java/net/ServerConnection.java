package net;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Listener.ThreadedListener;
import display.Options;
import display.WinController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import logic.Game;
import org.joml.Vector3f;

import java.awt.*;
import java.io.IOException;
import java.net.InetAddress;

import static net.NetworkProtocol.*;

public class ServerConnection {
  public Client client;
  public InetAddress ip;
  Game game;
  Boolean fullScreen;
  String name;
  Color color;
  String winner;
  Dimension vidmode = Toolkit.getDefaultToolkit().getScreenSize();
  int pid;
  Options options;

  public ServerConnection(Options options) {
    this.options = options;
    //default : Automatic connection
    client = new Client();
    client.start();
    NetworkProtocol.register(client);
    ip = client.discoverHost(54777, 5000);
  }

  public void connectAutomatic() {
    try {
      client.connect(5000, ip, 8989, 54777);
      startClientThread();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void connectManual(String ipString) {
    try {
      client.connect(5000, InetAddress.getByName(ipString), 8989, 54777);
      startClientThread();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  private void startClientThread() {
    // Connected to the Server, start the Listener
    //start the GameStarts
    client.addListener(
        new ThreadedListener(
            new Listener() {
              public void received(Connection connection, Object object) {
                if (object instanceof Winner) {
                  //Show the "player that won" + a funny line
                  //stop the game
                  winner = game.getHandler().players.get(((Winner) object).pid).name;
                  Platform.runLater(() -> {
                    switchToWin();
                  });
                }
                //set the player Object that was created on the server side to the player object
                if (object instanceof Login) {
                  //create new player
                  pid = ((Login) object).player.pid;
                }
                if (object instanceof UpdateObject) {
                  //update player
                  UpdateObject msg = (UpdateObject) object;
                  if (!msg.isDead) {
                    game.getHandler().players.forEach((pid, obj) -> {
                      if (pid == msg.player.pid) {
                        obj.posOnGrid = msg.player.posOnGrid;
                        obj.radius = msg.player.radius;
                      }
                    });
                  } else if (pid == msg.player.pid) {
                    //the player is this one
                    game.getHandler().players.remove(msg.player.pid);
                    //Show the "You died screen"
                    //stop the game for this, and open another
                    Platform.runLater(() -> {
                      switchToLose();
                    });
                  } else {
                    game.getHandler().players.remove(msg.player.pid);
                  }

                }
                if (object instanceof AddObject) {
                  //add the object to the right list
                  AddObject msg = (AddObject) object;

                  if (msg.player != null) {
                    //add player
                    game.getHandler().players.put(msg.player.pid, msg.player);
                  }
                  if (msg.point != null) {
                    //add point
                    game.getHandler().points.put(msg.point.pid, msg.point);
                  }
                }
                if (object instanceof RemoveObject) {
                  //remove the object from the right list
                  RemoveObject msg = (RemoveObject) object;
                  if (msg.player != null) {
                    //remove player
                    game.getHandler().players.remove(msg.player.pid);
                  }
                  if (msg.point != null) {
                    game.getHandler().points.remove(msg.point.pid);
                  }
                }
//                if (object instanceof LeaderBoard) {
//                  //set the concurrent Leaderboard to the pid's in this object
//                  if(game != null)
//                    if(game.getHud() != null)
//                      game.getHud().setLeaderboard(((LeaderBoard) object).pid);
//                }
              }

              public void disconnected(Connection connection) {
                System.exit(0);
              }
            }
        )
    );

    game = new Game(this, client, name, vidmode.width, vidmode.height);


    Login login = new Login();
    login.name = name;
    login.color = color;
    login.posOnScreen = new Vector3f(vidmode.width / 2, vidmode.height / 2, 0);
    client.sendTCP(login);
    while (game.getPID() == 0) try {
      game.setPID(pid);
    } catch (NullPointerException ignored) {
    }
    //start the game here
    game.start();

  }

  public void setColor(javafx.scene.paint.Color color) {
    this.color = new Color((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue());
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setFullscreen(Boolean fs) {
    this.fullScreen = fs;
  }

  public void setDimension(int w, int h) {
    this.vidmode.setSize(w, h);
  }

  public String getWinner() {
    return winner;
  }

  public void switchToLose() {
    //Load Fonts
    FXMLLoader fxml = new FXMLLoader(getClass().getResource("/fxml/loser.fxml"));
    Pane root = null;
    try {
      root = fxml.load();
    } catch (IOException e) {
      e.printStackTrace();
    }
    options.oc.getMainPane().getChildren().setAll(root);
    options.primaryStage.show();
  }

  public void switchToWin() {
    FXMLLoader fxml = new FXMLLoader(getClass().getResource("/fxml/winner.fxml"));
    Pane pane = null;
    try {
      pane = fxml.load();
    } catch (IOException e) {
      e.printStackTrace();
    }
    //get Controller
    WinController winController = fxml.getController();
    winController.setWinner(getWinner());
    options.oc.getMainPane().getChildren().setAll(pane);
    options.primaryStage.show();
  }
}
