package net;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;
import objects.Player;
import objects.Point;
import org.joml.Vector3f;

import java.awt.*;

public class NetworkProtocol {

  static public void register(EndPoint ep) {
    Kryo kryo = ep.getKryo();
    kryo.register(Vector3f.class, new Vector3Serializer());
    kryo.register(Color.class, new ColorSerialisation());
    kryo.register(int[].class);
    kryo.register(Player.class, new PlayerSerializer());
    kryo.register(Point.class, new PointSerializer());
    kryo.register(LeaderBoard.class);
    kryo.register(Winner.class);
    kryo.register(Login.class);
    kryo.register(AddObject.class);
    kryo.register(UpdateObject.class);
    kryo.register(RemoveObject.class);
    kryo.register(MoveObject.class);
  }

  static public class Disconnect {

  }

  static public class Login {
    public Player player;
    public Color color;
    public String name;
    public Vector3f posOnScreen;
    public int mapWidth;
    public int mapHeight;
  }

  static public class Winner {
    public int pid;
  }

  static public class UpdateObject {
    public Player player;
    public boolean isDead;
  }

  static public class AddObject {
    public Player player;
    public Point point;
  }

  static public class RemoveObject {
    public Player player;
    public Point point;
  }

  static public class MoveObject {
    public int pid;
    public Vector3f playerMousePosition;
  }

  static public class LeaderBoard {
    public int[] pid;
  }
}
