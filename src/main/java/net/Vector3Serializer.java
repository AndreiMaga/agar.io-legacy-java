package net;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.joml.Vector3f;

public class Vector3Serializer extends Serializer<Vector3f> {

  @Override
  public Vector3f read(Kryo kryo, Input in, Class<Vector3f> type) {
    return new Vector3f(in.readFloat(), in.readFloat(), in.readFloat());
  }

  @Override
  public void write(Kryo kryo, Output out, Vector3f vec) {
    out.writeFloat(vec.x);
    out.writeFloat(vec.y);
    out.writeFloat(vec.z);

  }
}
