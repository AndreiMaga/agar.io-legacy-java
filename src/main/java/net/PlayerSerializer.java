package net;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import objects.Player;
import org.joml.Vector3f;

import java.awt.*;

public class PlayerSerializer extends Serializer<Player> {

  @Override
  public void write(Kryo kryo, Output out, Player player) {
    out.writeString(player.name);
    out.writeFloat(player.posOnGrid.x);
    out.writeFloat(player.posOnGrid.y);
    out.writeFloat(player.posOnGrid.z);
    out.writeInt(player.pid);
    out.writeInt(player.radius);
    out.writeFloat((float) player.color.getRed() / 255);
    out.writeFloat((float) player.color.getGreen() / 255);
    out.writeFloat((float) player.color.getBlue() / 255);
    out.writeFloat(player.posOnScreen.x);
    out.writeFloat(player.posOnScreen.y);
  }

  @Override
  public Player read(Kryo kryo, Input in, Class<Player> type) {
    return new Player(in.readString(), new Vector3f(in.readFloat(), in.readFloat(), in.readFloat()), in.readInt(), in.readInt(), new Color(in.readFloat(), in.readFloat(), in.readFloat()));
  }
}
