package logic;

import com.esotericsoftware.kryonet.Client;
import graphics.Hud;
import graphics.Mouse;
import net.NetworkProtocol;
import net.ServerConnection;
import org.joml.Vector3f;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;


public class Game extends Canvas implements Runnable {

  Vector3f oldMousePos = new Vector3f(0);
  int pid;
  String name;
  Hud hud;
  Mouse mouse;
  MediaPlayer mediaPlayer;
  Dimension vidmode = new Dimension();
  private Client client;
  private ServerConnection sc;
  private Thread thread;
  private int updates = 0;
  private boolean running = false;
  private Handler handler = new Handler(vidmode);

  public Game(ServerConnection sc, Client client, String name, int width, int height) {
    this.sc = sc;
    this.vidmode.setSize(width, height);
    this.client = client;
    this.name = name;
  }

  public void start() {
    running = true;
    thread = new Thread(this, "Game");
    thread.start();
  }

  private void stop() {
    running = false;
    try {
      thread.join();
      mediaPlayer.stop();
      client.stop();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }


  private void init() {

    //Make JFrame
    JFrame display = new JFrame("Agar.io - Andrei Maga");
    display.setSize(vidmode);
    display.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    display.setResizable(false);
    display.setLocationRelativeTo(null);
    display.add(this);
    display.setVisible(true);

    //
    mouse = new Mouse();
    mediaPlayer = new MediaPlayer();
    this.addMouseMotionListener(mouse);
//    hud = new Hud(vidmode,handler);
    handler.setPID(pid);
    handler.players.get(pid).posOnScreen.x = vidmode.width / 2;
    handler.players.get(pid).posOnScreen.y = vidmode.height / 2;
    handler.players.get(pid).onScreen = true;
    mediaPlayer.start();
  }

  public void run() {
    init();
    long lastTime = System.nanoTime();
    double amountOfTicks = 60.0;
    long timer = System.currentTimeMillis();
    double ms = 10000000 / amountOfTicks;
    double delta = 0;
    while (running) {
      long now = System.nanoTime();
      delta += (now - lastTime) / ms;
      lastTime = now;
      while (delta >= 1) {
        update();
        delta--;
        updates++;
      }
      if (running)
        render();
      if (System.currentTimeMillis() - timer > 1000) {
        timer = System.currentTimeMillis();
        updates = 0;
      }
    }
    stop();
  }

  private void update() {
    handler.update();
    //send this every 20-30 updates
    if (updates % 20 == 0) {
      NetworkProtocol.MoveObject move = new NetworkProtocol.MoveObject();
      move.playerMousePosition = mouse.getMousePos();
      move.pid = pid;
      client.sendTCP(move);
    }
  }


  private void render() {
    BufferStrategy bs = this.getBufferStrategy();
    if (bs == null) {
      this.createBufferStrategy(3);
      return;
    }
    Graphics g = bs.getDrawGraphics();


    g.setColor(Color.white);
    g.fillRect(0, 0, vidmode.width, vidmode.height);

    handler.render(g);
//    hud.render(g);
    g.dispose();
    bs.show();

  }


  public Handler getHandler() {
    return handler;
  }

  public int getPID() {
    return pid;
  }

  public void setPID(int pid) {
    this.pid = pid;
    handler.setSize(vidmode.width, vidmode.height);
    handler.setPID(pid);
  }


  public Hud getHud() {
    return hud;
  }
}
