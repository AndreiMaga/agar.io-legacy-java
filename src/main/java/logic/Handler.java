package logic;

import objects.Player;
import objects.Point;
import org.joml.Vector3f;

import java.awt.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.joml.Math.abs;

public class Handler {
  public ConcurrentHashMap<Integer, Player> players = new ConcurrentHashMap<>();
  public ConcurrentHashMap<Integer, Point> points = new ConcurrentHashMap<>();
  int pid;
  Player player;
  Dimension size;
  double playerToEdges;

  public Handler(Dimension size) {
    this.size = size;
  }

  public void setPID(int pid) {
    this.pid = pid;
    player = players.get(pid);
    player.onScreen = true;
    calculateScreenSize();
  }

  private void calculateScreenSize() {
    playerToEdges = player.posOnScreen.distance((float) size.width, (float) size.height, 0f);
  }

  public void update() {
    player = players.get(pid);
    if (player != null) {
      players.forEach((pid, playerObject) -> {
        if (pid != player.pid) {
          //calculate the position on the screen with triangulation
          double distance = player.posOnGrid.distance(playerObject.posOnGrid);
          distance = abs(distance);

          if (distance + 100 < playerToEdges) {
            //set the positionOnScreen to  playerObject.x - player.x and playerObject.y - player.y
            playerObject.posOnScreen = new Vector3f(player.posOnScreen.x + playerObject.posOnGrid.x - player.posOnGrid.x, player.posOnScreen.y + playerObject.posOnGrid.y - player.posOnGrid.y, 0);
            playerObject.onScreen = true;
          } else {
            playerObject.onScreen = false;
          }

        } else {
          playerObject.onScreen = true;
          playerObject.posOnScreen = new Vector3f(size.width / 2, size.height / 2, 0);
        }

      });

      points.forEach((pid, playerObject) -> {
        //calculate the position on the screen with triangulation
        double distance = player.posOnGrid.distance(playerObject.posOnGrid);
        distance = abs(distance);

        if (distance + 12 < playerToEdges) {
          //set the positionOnScreen to  playerObject.x - player.x and playerObject.y - player.y
          playerObject.posOnScreen = new Vector3f(player.posOnScreen.x + playerObject.posOnGrid.x - player.posOnGrid.x, player.posOnScreen.y + playerObject.posOnGrid.y - player.posOnGrid.y, 0);
          playerObject.onScreen = true;
        } else {
          playerObject.onScreen = false;
        }
      });
    }

  }

  public void render(Graphics g) {
    //render the players and the points
    //don't render what is not in view of the camera
    // view of camera = (player.x + screenWidth/2 , player.y + screenHeight/2)
    players.forEach((pid, playerObject) -> {
      if (playerObject.onScreen)
        playerObject.render(g);
    });

    //get a random pid and show the pos on screen
    points.forEach((pid, point) -> {
      if (point.onScreen)
        point.render(g);
    });


  }

  public void setSize(int width, int height) {
    this.size.setSize(width, height);
  }
}
