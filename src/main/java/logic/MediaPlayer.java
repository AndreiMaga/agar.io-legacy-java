package logic;

import javazoom.jl.decoder.JavaLayerException;

import java.io.InputStream;
import java.util.Random;

public class MediaPlayer implements Runnable {
  private String[] songs = {
      "Clint",
      "Humble",
      "Deadpool",
      "Panda",
      "Queen",
      "Saturn",
      "Tiger"
  };
  private javazoom.jl.player.Player mediaPlayer;
  private boolean running = false;
  private Thread thread;


  @Override
  public void run() {
    while (running) {
      //play the song
      PlaySong(songs[new Random().nextInt(6)]);
    }
  }

  public void start() {
    running = true;
    thread = new Thread(this, "Media Player");
    thread.start();
  }

  public void stop() {
    running = false;
    try {
      thread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void PlaySong(String stringSong) {
    try {
      InputStream fis = ClassLoader.getSystemClassLoader().getResourceAsStream("songs/" + stringSong + ".mp3");
      mediaPlayer = new javazoom.jl.player.Player(fis);
      mediaPlayer.play();
    } catch (JavaLayerException e) {
      e.printStackTrace();
    }
  }

}
