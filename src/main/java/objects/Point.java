package objects;

import org.joml.Vector3f;

import java.awt.*;
import java.util.Random;

public class Point {
  public Vector3f posOnGrid;
  public Vector3f posOnScreen = new Vector3f(0);
  public int pid;
  public int radius = 12;
  public boolean onScreen = false;
  public Random rand = new Random();
  public Color color = new Color(rand.nextInt(150), rand.nextInt(150), rand.nextInt(150));

  public Point(int pid, Vector3f posOnGrid) {

    this.posOnGrid = posOnGrid;
    this.pid = pid;
  }

  public void render(Graphics g) {
    g.setColor(color);
    g.fillOval(
        (int) (this.posOnScreen.x - (this.radius / 2)),
        (int) (this.posOnScreen.y - (this.radius) / 2),
        this.radius, this.radius
    );
  }
}
