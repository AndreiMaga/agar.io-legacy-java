package objects;

import com.sun.javafx.collections.SortableList;
import org.joml.Vector3f;

import java.awt.*;


public class Player {
  public String name;
  public Vector3f posOnGrid;
  public Vector3f posOnScreen = new Vector3f(0);
  public int pid;
  public int radius;
  public boolean onScreen = false;
  public Color color;

  public Player(String name, Vector3f posOnGrid, int pid, int radius, Color color) {
    this.name = name;
    this.posOnGrid = posOnGrid;
    this.pid = pid;
    this.radius = radius;
    this.color = color;
  }


  public void render(Graphics g) {
    g.setColor(color);
    g.fillOval(
        (int) (this.posOnScreen.x - (this.radius / 2)),
        (int) (this.posOnScreen.y - (this.radius) / 2),
        this.radius, this.radius
    );
    g.drawString(this.name, (int) (posOnScreen.x - 20), (int) (posOnScreen.y + 70));
  }
}
