package display;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class WinController {

  @FXML
  private JFXButton endbutton;

  @FXML
  private Label winner;

  @FXML
  private Label playerWin;

  @FXML
  void closeGame(ActionEvent event) {
    System.exit(1);
  }

  public void setWinner(String winner) {
    this.playerWin.setText(winner);
  }
}
