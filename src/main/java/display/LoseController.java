package display;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class LoseController {

  @FXML
  private JFXButton exit;

  @FXML
  void closeGame(ActionEvent event) {
    System.exit(1);
  }

}
