package display;

import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import net.ServerConnection;

public class OptionsController {

  //Boolean that keeps track of the manual/automatic connection
  // ;; true = Manual
  // ;; false = Automatic
  boolean connectionStatus = true;
  //Get the server connection instance to get the state of the connection/to connect
  ServerConnection sc;
  Stage primaryStage;
  //Boolean that keeps track of the fullscreen or custom size
  // ;; true = custom
  // ;; false = fullscreen
  boolean switchSizes = true;
  @FXML
  private Pane MainPanel;

  @FXML
  private JFXTextField ip;
  @FXML
  private JFXButton next;
  @FXML
  private JFXSpinner spinner;
  @FXML
  private Label manual;
  @FXML
  private Label automatic;
  @FXML
  private Label wait;
  @FXML
  private JFXTextField nume;
  @FXML
  private JFXButton closeButton;
  @FXML
  private JFXToggleButton fullscreen;
  @FXML
  private JFXColorPicker color;
  @FXML
  private JFXButton connect;
  @FXML
  private JFXTextField height;
  @FXML
  private JFXTextField width;


  private boolean spinnerAppears = true;

  public Pane getMainPane() {
    return MainPanel;
  }

  public void setVariables(ServerConnection sc, Stage primaryStage) {
    this.sc = sc;
    this.primaryStage = primaryStage;
    if (sc.ip != null) {
      wait.setTextFill(Color.WHITE);
      wait.setText("Connected!");
      spinnerAppears = false;
    }
  }

  @FXML
  void closeGame(ActionEvent event) {
    Platform.exit();
  }

  @FXML
  void connect(ActionEvent event) {
    sc.setColor(color.getValue());
    sc.setName(nume.getText());
    //Connection
    if (switchSizes) {
      sc.setFullscreen(true);
    } else {
      sc.setFullscreen(false);
      sc.setDimension(Integer.parseInt(width.getText()), Integer.parseInt(height.getText()));
    }
    if (connectionStatus) {
      sc.connectManual(this.ip.getText());
    } else {
      sc.connectAutomatic();
    }
    //get the Connection status to destroy the window
    checkConn();

  }

  @SuppressWarnings("restriction")
  public void checkConn() {
    if (sc.client.isConnected()) {
      this.primaryStage.hide();
    }
  }


  @FXML
  void connectionSwitch(ActionEvent event) {
    if (connectionStatus) {
      //Automatic connection
      manual.setVisible(false);
      ip.setVisible(false);
      automatic.setVisible(true);
      wait.setVisible(true);
      if (spinnerAppears)
        spinner.setVisible(true);
    } else {
      //Manual Connection
      manual.setVisible(true);
      ip.setVisible(true);
      automatic.setVisible(false);
      wait.setVisible(false);
      spinner.setVisible(false);
    }

    connectionStatus = !connectionStatus;
  }

  @FXML
  void switchSize(ActionEvent event) {
    if (switchSizes) {
      //
      width.setVisible(true);
      height.setVisible(true);
    } else {
      width.setVisible(false);
      height.setVisible(false);
    }
    switchSizes = !switchSizes;
  }

}
