package display;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.ServerConnection;

public class Options extends Application {
  public OptionsController oc;
  public Stage primaryStage;
  ServerConnection sc;

  public static void main(String[] args) {
    Application.launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    this.primaryStage = primaryStage;
    //Open the ServerConnection so you can connect to the server
    sc = new ServerConnection(this);
    primaryStage.setTitle("Options");
    //Load Fonts
//    Font.loadFont(Objects.requireNonNull(ClassLoader.getSystemClassLoader().getResource("Roboto-Black.tff")).toExternalForm(),10);
    Scene scene;

    FXMLLoader fxml = new FXMLLoader(getClass().getResource("/fxml/agario.fxml"));
    Parent root = fxml.load();
    scene = new Scene(root);
    //get Controller
    oc = fxml.<OptionsController>getController();
    oc.setVariables(sc, primaryStage);

    primaryStage.setScene(scene);
    primaryStage.initStyle(StageStyle.UNDECORATED);
    primaryStage.setResizable(false);
    primaryStage.setWidth(600);
    primaryStage.setHeight(400);
    primaryStage.show();
  }

}
