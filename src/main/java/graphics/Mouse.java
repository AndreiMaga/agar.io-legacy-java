package graphics;

import org.joml.Vector3f;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class Mouse implements MouseMotionListener {

  private Vector3f mousePos = new Vector3f(0);

  @Override
  public void mouseDragged(MouseEvent e) {

  }

  @Override
  public void mouseMoved(MouseEvent e) {
    mousePos.x = e.getX();
    mousePos.y = e.getY();
  }

  public Vector3f getMousePos() {
    return mousePos;
  }
}
