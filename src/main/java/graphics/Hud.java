package graphics;

import logic.Handler;
import objects.Player;

import java.awt.*;

public class Hud {
  private Dimension screenSize;
  private Handler handler;
  private int[] leaderboard = new int[5];
  private int distance = 50;
  //head-up display class -> LeaderBoard / pos / points / names

  public Hud(Dimension screenSize, Handler handler) {
    this.screenSize = screenSize;
    this.handler = handler;
  }

  public void render(Graphics g) {
      //render the leaderboard in the top right
      for(int i =0; i<5; i++){
        Player player = handler.players.get(leaderboard[i]);
        g.setColor(player.color);
        g.drawString(i+") "+player.name+"    "+ player.radius,screenSize.width-100,distance*(i+1));
      }
  }

  public void setLeaderboard(int[] pids) {
    leaderboard = pids;
  }
}
